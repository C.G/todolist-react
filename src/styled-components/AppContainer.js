import styled from 'styled-components';

export default styled.div`
  font-family: 'Major Mono Display', monospace;
  display: flex;
  flex-flow: column wrap;
  align-items: center;
`;
