import styled from 'styled-components';

export default styled.div`
  display: flex;
  flex-flow: row wrap;
  justify-content: space-between;
  align-items: center;
  margin-top: 15px;
  border: 1px solid #3c3c3c;
  border-radius: 5px;
  padding: 10px;
`;
