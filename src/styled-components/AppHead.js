import styled from 'styled-components';

export default styled.div`
  display: flex;
  flex-flow: column wrap;
  align-items: center;
  background-color: #f5f5f5;
  width: 400px;
  padding: 20px;
  border-radius: 0px 0px 10px 10px;
`;
