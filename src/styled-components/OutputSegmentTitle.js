import styled from 'styled-components';

export default styled.p`
  font-family: 'Major Mono Display', monospace;
  font-size: 15px;
`;
