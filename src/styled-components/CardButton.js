import styled from 'styled-components';

export default styled.button`
  font-family: 'Major Mono Display', monospace;
  width: ${props => (props.primary ? '70px' : '55px')};
  height: 30px;
  margin-right: 5px;
`;
