import styled from 'styled-components';

export default styled.button`
  height: 37px;
  width: 50px;
  font-size: 15px;
  font-family: 'Major Mono Display', monospace;
  background-color: #f5f5f5;
  color: #3c3c3c;
  border: 1px solid #3c3c3c;
  border-radius: 5px;
  margin-left: 5px;
`;
