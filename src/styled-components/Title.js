import styled from 'styled-components';

export default styled.h1`
  font-weight: 400;
  color: #3c3c3c;
`;
