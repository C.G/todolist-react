import styled from 'styled-components';

export default styled.div`
  font-size: 15px;
  font-family: 'Major Mono Display', monospace;
  width: 200px;
  word-wrap: break-word;
`;
