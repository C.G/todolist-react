import styled from 'styled-components';

export default styled.input`
  width: 250px;
  height: 25px;
  font-family: 'Roboto', sans-serif;
  border: 1px solid #3c3c3c;
  border-radius: 5px;
  text-indent: 10px;
  padding: 5px;
  margin-right: 5px;
`;
