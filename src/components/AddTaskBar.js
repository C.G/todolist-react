import React from 'react';
import InputBar from '../styled-components/InputBar';
import InputButton from '../styled-components/InputButton';
import FormContainer from '../styled-components/FormContainer';

class AddTaskBar extends React.Component {
  state = { task: '' };

  onInputChange = event => {
    this.setState({ task: event.target.value });
  };

  onFormSubmit = event => {
    event.preventDefault();

    this.props.onTaskSubmit(this.state.task);
    this.setState({ task: '' });
  };

  render() {
    return (
      <FormContainer onSubmit={this.onFormSubmit}>
        <InputBar type="text" placeholder="Enter an activity.." value={this.state.task} onChange={this.onInputChange} />
        <InputButton type="submit">add</InputButton>
      </FormContainer>
    );
  }
}

export default AddTaskBar;
