import React from 'react';
import ToDoList from './ToDoList';
import AddTaskBar from './AddTaskBar';
import AppContainer from '../styled-components/AppContainer';
import AppHead from '../styled-components/AppHead';
import Title from '../styled-components/Title';
import OutputSegmentTitle from '../styled-components/OutputSegmentTitle';

class App extends React.Component {
  state = { tasks: [] };

  onTaskSubmit = task => {
    console.log(task);
    if (!task) {
      alert('Nie podano zadania!');
      return;
    }

    const newTask = {
      taskText: task,
      taskId: Date.now(),
      isDone: false
    };
    // Spread operator

    this.setState({
      tasks: [...this.state.tasks, newTask]
    });
  };

  deleteTask = event => {
    const tmpTasks = this.state.tasks;

    tmpTasks.forEach((task, index) => {
      if (task.taskId === event) {
        tmpTasks.splice(index, 1);
      }
    });

    this.setState({ tasks: tmpTasks });
  };

  makeTaskDone = event => {
    const tmpTasks = this.state.tasks;

    tmpTasks.forEach(task => {
      if (task.taskId === event) {
        task.isDone = true;
      }
    });

    this.setState({ tasks: tmpTasks });
  };

  render() {
    const tasksDone = this.state.tasks.filter(task => {
      return task.isDone === true;
    });

    const tasksToDo = this.state.tasks.filter(task => {
      return task.isDone === false;
    });

    return (
      <AppContainer>
        <AppHead>
          <Title>todo-list app</Title>
          <AddTaskBar onTaskSubmit={this.onTaskSubmit} />
        </AppHead>
        <div>
          <OutputSegmentTitle status="todo">tasks to do:</OutputSegmentTitle>
          <ToDoList tasks={tasksToDo} onRemoveClick={this.deleteTask} onDoneClick={this.makeTaskDone} />
          <OutputSegmentTitle status="done">tasks done:</OutputSegmentTitle>
          <ToDoList tasks={tasksDone} onRemoveClick={this.deleteTask} />
        </div>
      </AppContainer>
    );
  }
}

export default App;
