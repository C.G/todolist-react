import React from 'react';
import ToDoCard from './ToDoCard';
import ListContainer from '../styled-components/ListContainer';

const ToDoList = props => {
  const { tasks, onRemoveClick, onDoneClick } = props;

  const renderedList = tasks.map(task => {
    return (
      <ToDoCard
        key={task.taskId}
        taskText={task.taskText}
        taskId={task.taskId}
        isDone={task.isDone}
        onRemoveClick={() => onRemoveClick(task.taskId)}
        onDoneClick={() => onDoneClick(task.taskId)}
      />
    );
  });
  return <ListContainer>{renderedList}</ListContainer>;
};

export default ToDoList;
