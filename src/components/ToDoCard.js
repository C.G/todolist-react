import React from 'react';
import TaskCardContainer from '../styled-components/TaskCardContainer';
import TaskTextContainer from '../styled-components/TaskTextContainer';
import ButtonsContainer from '../styled-components/ButtonsContainer';
import CardButton from '../styled-components/CardButton';

const ToDoCard = props => {
  const { onRemoveClick, onDoneClick, taskId, taskText, isDone } = props;

  return (
    <TaskCardContainer>
      <TaskTextContainer>{taskText}</TaskTextContainer>
      <ButtonsContainer>
        <CardButton primary onClick={() => onRemoveClick(taskId)}>
          remove
        </CardButton>
        {!isDone ? <CardButton onClick={() => onDoneClick(taskId)}>done</CardButton> : null}
      </ButtonsContainer>
    </TaskCardContainer>
  );
};

export default ToDoCard;
